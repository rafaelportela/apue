# Advanced Programming in the UNIX Environment

To run examples and code from the book.

```
# http://www.apuebook.com/code3e.html
tar zxf src.3e.tar.gz
cd apue.e3
make
```

To compile the `printuid.c` example from chapter 1:

```
# cd chap01
gcc -o printuid printuid.c -I ../apue.3e/include/ -L ../apue.3e/lib/ -lapue
```

Where `-I` tells where to look for include files, `-L` is the location of the
lib directory, and `lapue` tells the name of the library file to look for in
that directory.

